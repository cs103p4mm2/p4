#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int showcount=0, dupsonly=0, uniqonly=0;
	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				showcount = 1;
				break;
			case 'd':
				dupsonly = 1;
				break;
			case 'u':
				uniqonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */
	if (showcount==0 && dupsonly==0 && uniqonly==0){
		string x;
		string y = "&#!@#!@^";
		while (getline(cin,x)){
			if (y==x) {
			}
			else if (y!=x) {
				cout << x << endl;
				y=x;
			}

		}
}
else if (showcount==1 && dupsonly==0 && uniqonly==0){
		string x;
		string y = "$!@#!%";
		int count = 0;
		while (getline(cin,x)){
			if (x==y) {
				count++;
			}

			else if (x!=y) {
			if (count ==0){
				y=x;
				count =1;
			}
			else if (count > 0 ){
				cout << count << " " << y << endl;
				count = 1;
				y=x;
			}

	
		}
		}

}
if (showcount == 0 && dupsonly == 1 && uniqonly == 0) {
string x;
	string y = "$!@#!@%";
	int count = 0;
	while (getline(cin,x)){
		if (x!=y){
		if (count > 0){
				cout << y << endl;
			count = 0;
			y =x;
		}
		else if (count == 0) {
			y=x;
		}
		}
		else if (x==y){
			count++;
		}

	}

}
else if (showcount==0 && dupsonly==0 && uniqonly==1){
		string x;
		string y = "&#!@#!@^";
	int count = 1;
		while (getline(cin,x)){
			if (y==x) {
				count=1;
			}
			else if (y!=x) {
				if (count == 1){
					y=x;
				count++;
			}
			else if (count > 1){
				cout << y << endl;
				count =1;
				y=x;

			}
		}

		}
}
else if (showcount==1 && dupsonly==1 && uniqonly==0){
		string x;
		string y = "&#!@#!@^";
		int count = 1;
		while (getline(cin,x)){
			if (x!=y){
				if (count > 1){
					cout << count << " " << y << endl;
					count = 1;
					y=x;
				}
				else if (count == 1){
					y=x;
				}
			}
			else if ( x==y) {
					count++;
			}
		}
	}

else if (showcount==1 && dupsonly==0 && uniqonly==1){
		string x;
		string y = "&#!@#!@^";
	int count = 1;
		while (getline(cin,x)){
			if (y==x) {
				count=1;
			}
			else if (y!=x) {
				if (count == 1){
					y=x;
				count++;
			}
			else if (count > 1){

				count =1;
				cout << count << " " << y << endl;
				y=x;

			}
		}

		}
}
return 0;
}
