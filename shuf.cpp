#include <cstdio>   // printf
#include <cstdlib>  // rand
#include <time.h>   // time
#include <getopt.h> // to parse long arguments.
#include <stdlib.h>
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <algorithm>
using std::swap;
using std::min;
#include <limits.h>
static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of shuf.  Supported options:\n\n"
"   -e,--echo              treat each argument as an input line.\n"
"   -i,--input-range=LO-HI treat each number in [LO..HI] as an input line.\n"
"   -n,--head-count=N      output at most N lines.\n"
"   --help                 show this message and exit.\n";
void printVec(vector<string> &a){
for(size_t i=0;i<a.size();i++){
cout << a[i] << endl;
	}
}


void printNumbVec(vector<int> &a){
for(size_t i=0;i<a.size();i++){
cout << a[i] << endl;
	}

	}
int main(int argc, char *argv[]) {
	// define long options
	static int echo=0, rlow=0, rhigh=0, useIflag=0,useNflag=0;    //added useIflag,useNflag so if theyre=1, that means that flag was used
	static size_t count=-1;
	bool userange = false;
	static struct option long_opts[] = {
		{"echo",        no_argument,       0, 'e'},
		{"input-range", required_argument, 0, 'i'},
		{"head-count",  required_argument, 0, 'n'},
		{"help",        no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "ei:n:h", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'e':
				echo = 1;
				break;
			case 'i':
				useIflag=1;
				if (sscanf(optarg,"%i-%i",&rlow,&rhigh) != 2) {
					fprintf(stderr, "Format for --input-range is N-M\n");
					rlow=0; rhigh=-1;
				} else {
					userange = true;
				}
				break;
			case 'n':
			useNflag=1;
				count = atol(optarg);
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	/* NOTE: the system's shuf does not read stdin *and* use -i or -e.
	 * Even -i and -e are mutally exclusive... */

	srand(time(0));
if(echo==1&& useIflag==0 && useNflag==0){     	//-e flag
vector <string> V;
while (optind < argc)
    V.push_back(argv[optind++]);           // pushback values that are echoed


size_t divider= V.size();                 //this is what im gonna divide rand with
vector<string> random;
for(size_t i=0;i<V.size();i++){
	random.push_back(V[rand()%divider]);      // push back a random index to random vector
	while(random[i]=="~"){
		random[i]=V[rand()%divider];
		}
for(size_t j=0;j<V.size();j++){                  // this makes sure that rand doesnt equal the same thing over and over
	if(random[i]==V[j]){
		V[j]= "~";
		}

	}

}
printVec(random);
}



else if(echo==0 && useIflag==0 && useNflag==0){                         // noflags
vector <string> V;
string x;
while (getline(cin,x))
//if(x=="")
//break;                                        // take out comment for testing
//else
V.push_back(x);           // pushback values that are echoed


size_t divider= V.size();
vector<string> random;
for(size_t i=0;i<V.size();i++){
	random.push_back(V[rand()%divider]);

	while(random[i]=="~"){
		random[i]=V[rand()%divider];
		}

	for(size_t j=0;j<V.size();j++){
	if(random[i]==V[j]){
		V[j]= "~";
		}

	}

}
printVec(random);
}



else if(echo==0 && useIflag==0 && useNflag==1){                  //-n flag
vector <string> V;
string x;
while (getline(cin,x)){
//if(x=="")break;                  //take out comment for manual testing
	//else
    V.push_back(x);

}
size_t divider= V.size();
vector<string> random;
for(size_t i=0;i<count;i++){
	random.push_back(V[rand()%divider]);

	while(random[i]=="~"){                 // from here onthis deals with rand not giving the same value
		random[i]=V[rand()%divider];
		}

	for(size_t j=0;j<V.size();j++){
	if(random[i]==V[j]){
		V[j]= "~";
		}

	}

}
printVec(random);
}





else if(echo==0 && useIflag==1 && useNflag==0){               //-i flag
vector <int> V;
int x=INT_MAX;
for(int i=rlow;i<=rhigh;i++){             // fill vector V with numbers from input range
V.push_back(i);
		}

size_t divider= V.size();
vector<int> random;
for(size_t i=0;i<V.size();i++){
	random.push_back(V[rand()%divider]);

	while(random[i]==x){
		random[i]=V[rand()%divider];
		}

	for(size_t j=0;j<V.size();j++){
	if(random[i]==V[j]){                          // this makes sure there are no repeating numbers. makes element old vec=INT_MAX for
		V[j]= x;
				}

			}

		}
printNumbVec(random);
	}
	return 0;
}
