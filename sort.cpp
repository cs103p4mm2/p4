
#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>

using std::string;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <set>
using std::set;
using std::multiset;
#include <strings.h>
#include <limits.h>
#include <vector>
using std::vector;
static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of sort.  Supported options:\n\n"
"   -r,--reverse        Sort descending.\n"
"   -f,--ignore-case    Ignore case.\n"
"   -u,--unique         Don't output duplicate lines.\n"
"   --help              Show this message and exit.\n";

struct igncaseComp {
	bool operator()(const string& s1, const string& s2) {
		return (strcasecmp(s1.c_str(),s2.c_str()) < 0);
	}
};
/* NOTE: set<string,igncaseComp> S; would declare a set S which
 * does its sorting in a case-insensitive way! */
void printVec(vector<string> &a){
for(size_t i=0;i<a.size();i++){
    cout << a[i] << endl;
}
    }


void printSet(set<string> &a){
for(set<string>::iterator i=a.begin();i!=a.end();i++){
 cout << *i << endl;

    }

}
void printSet(set<string,igncaseComp> &a){                                               //function overloading
for(set<string>::iterator i=a.begin();i!=a.end();i++){
 cout << *i << endl;

    }

}


void printMultiset (multiset <string,igncaseComp> &a){                                 // function overloading
for(multiset<string>::iterator i=a.begin();i!=a.end();i++){
 cout << *i << endl;

    }

}
void printMultiset (multiset <string> &a){                                 // function overloading. case sensitive multiset
for(multiset<string>::iterator i=a.begin();i!=a.end();i++){
 cout << *i << endl;

    }

}
int main(int argc, char *argv[]) {
	// define long options
	static int descending=0, ignorecase=0, unique=0;
	static struct option long_opts[] = {
		{"reverse",       no_argument,   0, 'r'},
		{"ignore-case",   no_argument,   0, 'f'},
		{"unique",        no_argument,   0, 'u'},
		{"help",          no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "rfuh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'r':
				descending = 1;
				break;
			case 'f':
				ignorecase = 1;
				break;
			case 'u':
				unique = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
if(unique==1 && descending==1 && ignorecase==0){    //if user does -u,-r
string x;                                                                 // for -u only (unique)
set <string> initial;                     // i chose to put input in set so all values can be unique
while(getline(cin,x)){               // read sentence and store
initial.insert(x);

}
vector<string> Sorted;
for(set<string>::iterator i=initial.begin();i!=initial.end();i++){  // i now put the unique lines in a vector so I can sort it
    Sorted.push_back(*i);
}


for(size_t i=0;i<Sorted.size();i++){
	cout << Sorted[(Sorted.size()-1) -i] << endl;

	}

		}



else if(unique==1 && descending==0 && ignorecase==0){    // if user uses -u command.works.

string x;                                                                 // for -u only (unique)
set <string> initial;                     // i chose to put input in set so all values can be unique
while(getline(cin,x)){               // read sentence and store

initial.insert(x);

		}
printSet(initial);

}



else if(descending==1 && unique==0 && ignorecase==0){               // -r option. works.
string x;
multiset<string> initial;
while(getline(cin,x)){
initial.insert(x);
	}
vector <string> descend;
for(set<string>::iterator i=initial.begin();i!=initial.end();i++){          // populates in vec so i can print backwards
	descend.push_back(*i);
	}
for(size_t i=0;i<descend.size();i++){
cout << descend[(descend.size()-1)-i] << endl;

	}
}



else if(ignorecase==1 && descending==0 && unique==0){               //-f option,works.
multiset<string,igncaseComp> S;      // multiset to store dupicaltes,igncaseComp to sort case insensitive
string x;
while(getline(cin,x)){

S.insert(x);
}

printMultiset(S);


	}



else if(ignorecase==1 && descending==1 && unique==1){   // -f - u -r, works.
set<string,igncaseComp> S;                    // takes care of -f,-u
string x;
while(getline(cin,x)){

S.insert(x);
}
vector <string> allFlags;                       // will put stuff from set in vector so I can print backwards
for(set<string>::iterator i=S.begin();i!=S.end();i++){
	allFlags.push_back(*i);

		}
for(size_t i=0;i<allFlags.size();i++){               // print vec backwards
cout << allFlags[(allFlags.size()-1)-i] << endl;

	}
}


else if(ignorecase==1 && descending==0 && unique==1){   // -u -f flags,works.
set<string,igncaseComp> S;      // multiset to store dupicaltes,igncaseComp to sort case insensitive
string x;
while(getline(cin,x)){

S.insert(x);
}
printSet(S);


	}




else if(ignorecase==1 && descending==1 && unique==0){             // -r -f flags
multiset<string,igncaseComp> S;      // multiset to store dupicaltes,igncaseComp to sort case insensitive
string x;
while(getline(cin,x)){
S.insert(x);
}
vector <string> unique;                               // populating vec to print backwards
for(set<string>::iterator i=S.begin();i!=S.end();i++){
unique.push_back(*i);
				}
for(size_t i=0;i<unique.size();i++){

	cout << unique[(unique.size()-1)-i] << endl;

	}


}



else if (ignorecase==0 && descending==0 && unique==0){ // no flags
multiset<string> S;      // multiset to store dupicaltes,igncaseComp to sort case insensitive
string x;
while(getline(cin,x)){
S.insert(x);
}
printMultiset(S);

}


	return 0;
}
